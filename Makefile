#!/usr/bin/make -f

files := gottengeography setup.py gg tests

build:
	python3 setup.py build

check: nose flakes pep8

doctest:
	python3 -m nose --with-doctest gg/ -v

nose:
	python3 -m coverage run -m nose2
	python3 -m coverage report --include='g*/*,t*/*' --show-missing --fail-under=85

flakes:
	python3 -m pyflakes $(files)

pep8:
	python3 -m pep8 $(files) --exclude=territories.py,version.py --ignore=E402,E731

lint:
	# FIXME: python3.4 hardcoded because pylint is broken in python3.5: https://bitbucket.org/logilab/astroid/issues/187/call-object-has-no-attribute-starargs
	python3.4 -m pylint $(files) --disable attribute-defined-outside-init,broad-except,fixme,import-error,invalid-name,no-init,no-member,no-self-use,no-value-for-parameter,protected-access,star-args,too-few-public-methods,too-many-arguments,too-many-instance-attributes,too-many-public-methods,unbalanced-tuple-unpacking,unused-argument,unused-variable

install:
	python3 setup.py install

clean:
	rm -rf build/ *.egg-info/

cities:
	wget -t 10 'http://download.geonames.org/export/dump/cities1000.zip'
	unzip -u cities1000.zip
	./tools/update_cities.py cities1000.txt > data/cities.txt
	rm -f cities1000.*

territories:
	wget -t 10 'http://download.geonames.org/export/dump/countryInfo.txt'
	wget -t 10 'http://download.geonames.org/export/dump/admin1CodesASCII.txt'
	./tools/update_territories.py > gg/territories.py
	rm -f countryInfo.txt admin1CodesASCII.txt

translations:
	bzr branch lp:~gottengeography/gottengeography/translations
	mv translations/po/*.po po/
	rm -rf translations
